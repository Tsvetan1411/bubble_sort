import random
import string

def generate_random_strings(num_strings, min_length, max_length):
    random_strings = []

    for a in range(num_strings):
        length = random.randint(min_length, max_length)
        random_str = ''.join(random.choices(string.ascii_letters, k=length))
        random_strings.append(random_str)
    return random_strings

def bubble_sort(str_list=None):
    str_list = generate_random_strings(10, 1, 5) if str_list == None else str_list
    for i in range(len(str_list)):
        for j in range(len(str_list) - 1 - i):
            if str_list[j] > str_list[j+1]:
                str_list[j], str_list[j+1] = str_list[j+1], str_list[j]
    return str_list

str_list = bubble_sort()
print(str_list)